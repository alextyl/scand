package com.junior.scand.model;

/**
 * Created by Алексей on 27.02.2018.
 */

public class RoboThread extends Thread implements Runnable {

    private Robot robot;

    public RoboThread(Robot robot){
        this.robot = robot;
    }

    @Override
    public void run() {
        this.robot.work();
    }
}
