package com.junior.scand.model;

import android.graphics.Point;

public class Box {


    private Point currentPosition;
    private boolean hasPersonalRobot;

    @Override
    public int hashCode() {
        return currentPosition.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(this.getClass() != obj.getClass()) return false;
        Box box = (Box) obj;
        return this.currentPosition.equals(box.currentPosition) && this.hasPersonalRobot == box.hasPersonalRobot;
    }

    Box(Point currentPosition){
        this.currentPosition = currentPosition;
    }

    public Point getCurrentPosition() {
        return currentPosition;
    }

    void setCurrentPosition(Point currentPosition) {
        this.currentPosition = currentPosition;
    }

    public boolean isHasPersonalRobot() {
        return hasPersonalRobot;
    }

    void setHasPersonalRobot(boolean hasPersonalRobot) {
        this.hasPersonalRobot = hasPersonalRobot;
    }
}