package com.junior.scand.model;


public interface RobotMoveListener {

    void robotMoveReaction(Robot robot);

}
