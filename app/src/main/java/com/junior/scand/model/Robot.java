package com.junior.scand.model;

import android.graphics.Point;

import java.util.*;

public class Robot {

    private Point currentPosition;
    private Stack<Point> robotPath;
    private Stack<Point> boxPath;
    private Box ownBox;
    private ArrayList<RobotMoveListener> robotMoveListeners = new ArrayList<>();


    public Robot(Point currentPosition){
        this.currentPosition = currentPosition;
    }

    public void work(){
        while (true){
            Box box = MapInfo.getInstance().getFreeBox();
            if (box == null) break;
            box.setHasPersonalRobot(true);
            takeBox(box);
            moveBoxPath();
        }
        moveTo(MapInfo.getInstance().getStart());
    }

    private void moveTo(Point finish){
        robotPath = createRobotPath(finish);
        while (!robotPath.empty()){
            currentPosition =  robotPath.pop();
            sendMove();
        }
    }

    private void moveToWithBox(Point finish){
        robotPath = createRobotPath(finish);
        while (!robotPath.empty()){
            Point oldPosition = new Point(currentPosition.x, currentPosition.y);
            currentPosition =  robotPath.pop();
            int x = oldPosition.x - currentPosition.x;
            int y = oldPosition.y - currentPosition.y;
            this.ownBox.setCurrentPosition(new Point(ownBox.getCurrentPosition().x - x, ownBox.getCurrentPosition().y - y));
            sendMove();
        }
    }

    private void sendMove(){
        for (RobotMoveListener robotMoveListener : robotMoveListeners){
            robotMoveListener.robotMoveReaction(this);
        }
    }

    private void createBoxPath(){
        boxPath = createBoxPath(MapInfo.getInstance().getFinish());
    }

    private void takeBox(Box box){
        this.ownBox = box;
    }

    private void moveBoxPath(){
        createBoxPath();

        Integer currentRoadTurn = -1;
        Integer currentRoadTurnLong = 0;
        Direction currentDirection = Direction.Default;
        HashMap<Integer, HashMap<Direction, Integer>> road = new HashMap<>();

        Point firtsPoint = ownBox.getCurrentPosition();
        Point secondPoint = boxPath.pop();

        while (true){
            if(firtsPoint.x < secondPoint.x){
                if(currentDirection == Direction.Right){
                    currentRoadTurnLong++;
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                } else {
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                    currentRoadTurn++;
                    currentRoadTurnLong = 1;
                    currentDirection = Direction.Right;
                }
            } else
            if(firtsPoint.x > secondPoint.x){
                if(currentDirection == Direction.Left){
                    currentRoadTurnLong++;
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                } else {
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                    currentRoadTurn++;
                    currentRoadTurnLong = 1;
                    currentDirection = Direction.Left;
                }
            } else
            if(firtsPoint.y < secondPoint.y){
                if(currentDirection == Direction.Down){
                    currentRoadTurnLong++;
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                } else {
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                    currentRoadTurn++;
                    currentRoadTurnLong = 1;
                    currentDirection = Direction.Down;
                }
            } else
            if(firtsPoint.y > secondPoint.y){
                if(currentDirection == Direction.Up){
                    currentRoadTurnLong++;
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                } else {
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                    currentRoadTurn++;
                    currentRoadTurnLong = 1;
                    currentDirection = Direction.Up;
                }
            }
            if(!boxPath.empty()){
                firtsPoint = secondPoint;
                secondPoint = boxPath.pop();
            } else {
                if(currentRoadTurnLong == 1){
                    HashMap<Direction, Integer> currentRoad = new HashMap<>();
                    currentRoad.put(currentDirection, currentRoadTurnLong);
                    road.put(currentRoadTurn, currentRoad);
                }
                break;
            }
        }


        for(int i = 0; i < road.size() - 1; i++){
            HashMap<Direction, Integer> map = road.get(i);
            Map.Entry<Direction, Integer> entryIn = map.entrySet().iterator().next();
                int x = this.ownBox.getCurrentPosition().x;
                int y = this.ownBox.getCurrentPosition().y;
                this.moveTo(new Point(x - entryIn.getKey().getStepByOx(), y - entryIn.getKey().getStepByOy()));
                x = this.currentPosition.x;
                y = this.currentPosition.y;
                this.moveToWithBox(new Point(x + (entryIn.getKey().getStepByOx()) * entryIn.getValue(),
                        y + (entryIn.getKey().getStepByOy()) * entryIn.getValue()));

        }


    }

    private Stack<Point> createRobotPath(Point finish){
        return createPath(currentPosition, finish, true);
    }

    private Stack<Point> createBoxPath(Point finish){
        return createPath(ownBox.getCurrentPosition(), finish, false);
    }

    private Stack<Point> createPath(Point start, Point finish, boolean isRobot){

        Stack<Point> path = new Stack<>();

        if(start.equals(finish)) return path;

        Integer currentRange = 0;
        HashSet<Point> currentPoints = new HashSet<>();
        currentPoints.add(start);
        Map<Integer, HashSet<Point>> rangeOfReach = new HashMap<>();
        rangeOfReach.put(currentRange, currentPoints);

        while (!rangeOfReach.get(currentRange).contains(finish)){
            HashSet<Point> buffer = new HashSet<>();

            for (Point point: rangeOfReach.get(currentRange)){
                for(int x = -1; x <= 1; x++) {
                    for(int y = -1; y <= 1; y++) {
                        if(Math.abs(x) - Math.abs(y) == 0) {
                            continue;
                        }
                        if(point.x >= 0 && point.y >= 0 && !MapInfo.getInstance().getBlocks().contains(new Point(point.x + x, point.y + y))
                                && (isRobot || !checkIsLock(new Point(point.x + x, point.y + y)))){
                            buffer.add(new Point(point.x + x, point.y + y));
                        }
                    }
                }
            }
            rangeOfReach.put(++currentRange, buffer);
        }

        Point point = new Point(finish.x, finish.y);
        path.push(finish);

        for(; currentRange > 0; currentRange--){
            HashSet<Point> points = rangeOfReach.get(currentRange);

            for(int x = -1; x <= 1; x++) {
                for(int y = -1; y <= 1; y++) {
                    if(Math.abs(x) - Math.abs(y) == 0) {
                        continue;
                    }
                    if(points.contains(new Point(point.x + x, point.y + y))){
                        path.push(new Point(point.x + x, point.y + y));
                        point = new Point(point.x + x, point.y + y);
                    }
                }
            }
        }

        return path;

    }

    private boolean checkIsLock(Point point) {
        for(int x = -1; x <= 1; x += 2) {
            for(int y = -1; y <= 1; y += 2) {
                if((MapInfo.getInstance().getBlocks().contains(new Point(point.x + x, point.y))
                        && MapInfo.getInstance().getBlocks().contains(new Point(point.x, point.y + y))
                        && !MapInfo.getInstance().getFinish().equals(point))){
                    return true;
                }
            }
        }
        return false;
    }

    public Point getCurrentPosition() {
        return currentPosition;
    }


    public void setRobotMoveListeners(RobotMoveListener robotMoveListeners) {
        this.robotMoveListeners.add(robotMoveListeners);
    }

}
