package com.junior.scand.model.json;


import android.graphics.Point;

import com.junior.scand.model.Box;

import java.util.ArrayList;
import java.util.HashSet;

public class JsonInfo {

    private Point start;
    private Point finish;
    private ArrayList<Box> boxes;
    private HashSet<Point> blocks;

    public JsonInfo(Point start, Point finish, ArrayList<Box> boxes, HashSet<Point> blocks) {
        this.start = start;
        this.finish = finish;
        this.boxes = boxes;
        this.blocks = blocks;
    }

    public JsonInfo(){}

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getFinish() {
        return finish;
    }

    public void setFinish(Point finish) {
        this.finish = finish;
    }

    public ArrayList<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(ArrayList<Box> boxes) {
        this.boxes = boxes;
    }

    public HashSet<Point> getBlocks() {
        return blocks;
    }

    public void setBlocks(HashSet<Point> blocks) {
        this.blocks = blocks;
    }
}
