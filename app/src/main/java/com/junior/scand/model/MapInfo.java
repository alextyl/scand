package com.junior.scand.model;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;

import com.google.gson.Gson;
import com.junior.scand.App;
import com.junior.scand.R;
import com.junior.scand.model.json.JsonInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;


public class MapInfo {

    private static MapInfo ourInstance;

    private HashSet<Point> blocks = new HashSet<>();
    private ArrayList<Box> boxes = new ArrayList<>();
    private ArrayList<Robot> robots = new ArrayList<>();
    private Point start = new Point();
    private Point finish = new Point();

    public static MapInfo getInstance() {
        return ourInstance == null ? ourInstance = getMapInfo() : ourInstance;
    }

    public void restart(){
        ourInstance = getMapInfo();
    }

    private MapInfo(HashSet<Point> blocks, ArrayList<Box> boxes, Point start, Point finish) {
        this.blocks = blocks;
        this.boxes = boxes;
        this.start = start;
        this.finish = finish;
    }


    private static MapInfo getMapInfo(){

        Gson gson = new Gson();
        BufferedReader r = new BufferedReader(
                new InputStreamReader(App.getContext().getResources().openRawResource(R.raw.map)));
        StringBuilder json = new StringBuilder();
        String line;
        try {
            while ((line = r.readLine()) != null) {
                json.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonInfo jsonInfo = gson.fromJson(json.toString(), JsonInfo.class);

        return new MapInfo(jsonInfo.getBlocks(),
                jsonInfo.getBoxes(),
                jsonInfo.getStart(),
                jsonInfo.getFinish());
    }

    synchronized Box getFreeBox(){
        for(Box box : boxes){
            if(!box.isHasPersonalRobot()) return box;
        }
        return null;
    }

    public HashSet<Point> getBlocks() {
        return blocks;
    }

    public void setBlocks(HashSet<Point> blocks) {
        this.blocks = blocks;
    }

    public ArrayList<Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(ArrayList<Box> boxes) {
        this.boxes = boxes;
    }

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getFinish() {
        return finish;
    }

    public void setFinish(Point finish) {
        this.finish = finish;
    }

    public ArrayList<Robot> getRobots() {
        return robots;
    }

    public MapInfo addRobot(Robot robot) {
        robots.add(robot);
        return this;
    }
}

