package com.junior.scand;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;

import com.junior.scand.model.Box;
import com.junior.scand.model.MapInfo;
import com.junior.scand.model.RoboThread;
import com.junior.scand.model.Robot;
import com.junior.scand.model.RobotMoveListener;


public class GameActivity extends AppCompatActivity {

    private static final int ROW = 8;
    private static final int COL = 15;

    private ImageAdapter imageAdapter;
    private Button start;
    private Button restart;
    private Handler mHandler;
    private EditText editText;
    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        GridView gridView = (GridView) findViewById(R.id.grid_view);
        imageAdapter = new ImageAdapter(this, COL * ROW);
        gridView.setAdapter(imageAdapter);

        for(Box box : MapInfo.getInstance().getBoxes()){
            Point point = box.getCurrentPosition();
            imageAdapter.mThumbIds[point.y * COL + point.x] = R.drawable.box;
        }

        for(Point block : MapInfo.getInstance().getBlocks()){
            imageAdapter.mThumbIds[block.y * COL + block.x] = R.drawable.block;
        }

        Point start = MapInfo.getInstance().getStart();
        Point finish = MapInfo.getInstance().getFinish();

        imageAdapter.mThumbIds[start.y * COL + start.x] = R.drawable.start;
        imageAdapter.mThumbIds[finish.y * COL + finish.x] = R.drawable.finish;

        imageAdapter.notifyDataSetChanged();

        this.start = findViewById(R.id.play);
        this.start.setOnClickListener(view -> play());

        this.restart = findViewById(R.id.play_restart);
        this.restart.setOnClickListener(view -> {MapInfo.getInstance().restart();
        finish();
        startActivity(intent);});

        mHandler = new Handler() {
            public void handleMessage(android.os.Message msg) {
                imageAdapter.notifyDataSetChanged();
            };
        };

        intent = getIntent();
        editText = findViewById(R.id.robot_count);

    }

    public void play(){

        int count;
        try {
            count = Integer.parseInt(editText.getText().toString());
            if(count < 2) count = 2;
            if(count > 5) count = 5;
        } catch (NumberFormatException e){
            e.getStackTrace();
            count = 2;
        }

        for (int i = 0; i < count; i++){
            Robot robot = new Robot(MapInfo.getInstance().getStart());
            MapInfo.getInstance().addRobot(robot);
        }

        for (Robot r : MapInfo.getInstance().getRobots()){
            r.setRobotMoveListeners(robotMoveListener);

            RoboThread roboThread = new RoboThread(r);
            roboThread.start();
        }


    }


    private RobotMoveListener robotMoveListener = new RobotMoveListener() {
        @Override
        public void robotMoveReaction(Robot robot) {

                for(int i = 0; i < imageAdapter.mThumbIds.length; i++){
                    imageAdapter.mThumbIds[i] = R.drawable.back;
                }

                for(Box box : MapInfo.getInstance().getBoxes()){
                    Point point = box.getCurrentPosition();
                    imageAdapter.mThumbIds[point.y * COL + point.x] = R.drawable.box;
                }

                for(Robot robot2 : MapInfo.getInstance().getRobots()){
                    Point point = robot2.getCurrentPosition();
                    imageAdapter.mThumbIds[point.y * COL + point.x] = R.drawable.robot;
                }


                for(Point block : MapInfo.getInstance().getBlocks()){
                    imageAdapter.mThumbIds[block.y * COL + block.x] = R.drawable.block;
                }


                Point start = MapInfo.getInstance().getStart();
                Point finish = MapInfo.getInstance().getFinish();

                imageAdapter.mThumbIds[start.y * COL + start.x] = R.drawable.start;
                imageAdapter.mThumbIds[finish.y * COL + finish.x] = R.drawable.finish;

                mHandler.sendEmptyMessage(0);


            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    };



    private class ImageAdapter extends BaseAdapter {

        private Context mContext;

        private Integer[] mThumbIds = {
        };

        private ImageAdapter(Context c, int size) {
            mContext = c;
            mThumbIds = new Integer[size];
            for(int i = 0; i < size; i++){
                mThumbIds[i] = R.drawable.back;
            }
        }

        @Override
        public int getCount() {
            return mThumbIds.length;
        }

        @Override
        public Object getItem(int position) {
            return mThumbIds[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView imageView = new ImageView(mContext);
            imageView.setImageResource(mThumbIds[position]);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setLayoutParams(new GridView.LayoutParams(100, 100));
            return imageView;
        }

    }
}

